/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '@/bootstrap'
import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import router from '@/router'
import vuetify from '@/vuetify'
import store from '@/store'
import App from '@/App.vue'

Vue.use(Vuetify)
Vue.use(VueRouter)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/**
 * ログインしている場合はアクセストークンを付与する
 */
if (store.getters['auth/isLogin']) {
    window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.state.auth.token
}

new Vue({
    el: '#app',
    router,
    vuetify,
    store,
    components: { App },
    template: '<App />',
})
