# Laravel Vuetify Starter

## Included
* [Laravel 6.0](https://laravel.com/docs/6.0)
* [Vue 2](https://vuejs.org)
* [Vue Router 3](http://router.vuejs.org)
* [Vuex 3](http://vuex.vuejs.org)
* [Axios](https://github.com/mzabriskie/axios)
* [Authentication with JWT Token](https://github.com/tymondesigns/jwt-auth)
* [Vuetify](https://vuetifyjs.com/en/getting-started/quick-start)
* [php-cs-fixer](https://cs.symfony.com/)
* [js-beautify](https://github.com/beautify-web/js-beautify)
* [eslint](https://eslint.org/)
* [prettier](https://prettier.io/)
* [husky](https://github.com/typicode/husky)
* [lint-staged](https://github.com/okonet/lint-staged)

## Installation:
* Clone the repository
* Copy `.env.example` to `.env`
* Configure `.env`
* Run `composer install`
* Run `php artisan key:generate`
* Run `php artisan jwt:secret`
* Run `php artisan migrate`
* Run `yarn install`

## Usage
* Run `yarn watch` for live reloading using BrowserSync
* Run `yarn hot` for hot reloading
* Run `yarn prod` for production buid
